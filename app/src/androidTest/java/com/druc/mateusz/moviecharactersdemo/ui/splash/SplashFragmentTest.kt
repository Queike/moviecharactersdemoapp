package com.druc.mateusz.moviecharactersdemo.ui.splash

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.druc.mateusz.moviecharactersdemo.R
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class SplashFragmentTest {

    @Test
    fun test_isSplashScreenMainTextVisible() {
        launchFragmentInContainer<SplashFragment>()
        onView(withId(R.id.splashTextView)).check(matches(isDisplayed()))
        onView(withId(R.id.splashTextView)).check(matches(withText(R.string.splash_screen_label)))
    }
}