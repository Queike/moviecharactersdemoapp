package com.druc.mateusz.moviecharactersdemo.ui.characterDetails

import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.druc.mateusz.moviecharactersdemo.R
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class CharacterDetailsFragmentTest {

    @Test
    fun test_areMovieCharacterDetailsVisible() {
        val bundle = Bundle()
        bundle.putString("characterName", "Test Character Name")
        bundle.putString("characterPhotoUrl", "https://image.freepik.com/free-icon/important-person_318-10744.jpg")
        bundle.putString("characterDescription", "Test character description.")
        launchFragmentInContainer<CharacterDetailsFragment>(bundle)

        Espresso.onView(ViewMatchers.withId(R.id.characterImageView))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.characterNameTextView))
            .check(ViewAssertions.matches(ViewMatchers.withText("Test Character Name")))
        Espresso.onView(ViewMatchers.withId(R.id.characterDescriptionTextView))
            .check(ViewAssertions.matches(ViewMatchers.withText("Test character description.")))
    }
}