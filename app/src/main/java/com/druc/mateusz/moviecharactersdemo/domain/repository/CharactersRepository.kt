package com.druc.mateusz.moviecharactersdemo.domain.repository

import com.druc.mateusz.moviecharactersdemo.api.MovieCharactersApi
import com.druc.mateusz.moviecharactersdemo.api.model.MovieCharacterEntity
import com.druc.mateusz.moviecharactersdemo.common.ErrorMessageProvider
import com.druc.mateusz.moviecharactersdemo.common.toDomain
import com.druc.mateusz.moviecharactersdemo.domain.model.MovieCharacter
import com.druc.mateusz.moviecharactersdemo.common.Result
import java.lang.Exception

class CharactersRepository(
    private val api: MovieCharactersApi,
    private val errorMessageProvider: ErrorMessageProvider
) {

    suspend fun getMovieCharacters(): Result<List<MovieCharacter>> {
        val characters: List<MovieCharacterEntity>

        return try {
            characters = api.getMovieCharacters().data
            Result.Success(characters.toDomain())
        } catch (exception: Exception) {
            Result.Failure(errorMessageProvider.getMessageForApiException(exception))
        }
    }
}