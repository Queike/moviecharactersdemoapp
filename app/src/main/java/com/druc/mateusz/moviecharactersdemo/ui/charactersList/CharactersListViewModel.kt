package com.druc.mateusz.moviecharactersdemo.ui.charactersList

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.druc.mateusz.moviecharactersdemo.BaseViewModel
import com.druc.mateusz.moviecharactersdemo.NavigationDispatcher
import com.druc.mateusz.moviecharactersdemo.domain.model.MovieCharacter
import com.druc.mateusz.moviecharactersdemo.domain.repository.CharactersRepository
import com.druc.mateusz.moviecharactersdemo.common.Result
import kotlinx.coroutines.launch

class CharactersListViewModel(
    private val navigationDispatcher: NavigationDispatcher,
    private val charactersRepository: CharactersRepository
): BaseViewModel() {

    private val _isLoadingLiveData = MutableLiveData<Boolean>()
    val isLoadingLiveData: LiveData<Boolean>
        get() = _isLoadingLiveData

    private val _charactersLiveData = MutableLiveData<List<MovieCharacter>>()
    val charactersLiveData: LiveData<List<MovieCharacter>>
        get() = _charactersLiveData

    init {
        fetchCharactersData()
    }

    fun navigateToItemDetails(item: MovieCharacter) {
        navigationDispatcher.navigateToCharacterDetails(
            item.name,
            item.photoUrl,
            item.characterDescription
        )
    }

    private fun fetchCharactersData() {
        _isLoadingLiveData.value = true
        viewModelScope.launch {
            when(val charactersData = charactersRepository.getMovieCharacters()) {
                is Result.Success -> {
                    charactersData.response.let {
                        _charactersLiveData.value = it
                    }
                }
                is Result.Failure -> {
                    errorLiveData.value = charactersData.errorMessage
                }
            }
            _isLoadingLiveData.value = false
        }
    }
}