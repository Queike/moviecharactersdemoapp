package com.druc.mateusz.moviecharactersdemo.api.model

import com.google.gson.annotations.SerializedName

data class MovieCharacterEntity(
    @SerializedName("firstname")
    val firstName: String,
    @SerializedName("lastname")
    val lastName: String,
    @SerializedName("photoUrl")
    val photoUrl: String,
    @SerializedName("description")
    val characterDescription: String,
)