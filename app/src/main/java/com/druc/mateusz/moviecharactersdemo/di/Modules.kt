package com.druc.mateusz.moviecharactersdemo.di

import com.druc.mateusz.moviecharactersdemo.NavigationDispatcher
import com.druc.mateusz.moviecharactersdemo.api.createBasicAuthService
import com.druc.mateusz.moviecharactersdemo.common.ErrorMessageProvider
import com.druc.mateusz.moviecharactersdemo.domain.repository.CharactersRepository
import com.druc.mateusz.moviecharactersdemo.ui.characterDetails.CharacterDetailsViewModel
import com.druc.mateusz.moviecharactersdemo.ui.charactersList.CharactersListViewModel
import com.druc.mateusz.moviecharactersdemo.ui.splash.SplashViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val networkModule = module {
    single { createBasicAuthService() }
}

val appModule = module {
    single { NavigationDispatcher() }
    single { ErrorMessageProvider(get()) }
}

val repositoryModule = module {
    single { CharactersRepository(get(), get()) }
}

val viewModelModule = module {
    viewModel { SplashViewModel(get()) }
    viewModel { CharactersListViewModel(get(), get()) }
    viewModel { CharacterDetailsViewModel() }
}