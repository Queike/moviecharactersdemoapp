package com.druc.mateusz.moviecharactersdemo.api

import com.druc.mateusz.moviecharactersdemo.api.model.Data
import com.druc.mateusz.moviecharactersdemo.api.model.MovieCharacterEntity
import retrofit2.http.GET

interface MovieCharactersApi {

    @GET("/characters")
    suspend fun getMovieCharacters(): Data<List<MovieCharacterEntity>>
}