package com.druc.mateusz.moviecharactersdemo.common

import android.content.Context
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter(value = ["android:src", "context", "circular"], requireAll = true)
fun setImageUrl(imageView: ImageView, imageUrl: String, context: Context, circular: Boolean) {
    if (imageUrl.isNotEmpty()) {
        when(circular) {
            true -> imageView.loadOvalWithThumbnailFromUrl(imageUrl, context)
            else -> imageView.loadFromUrl(imageUrl, context)
        }
    }
}

@BindingAdapter("visibleIf")
fun setVisible(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}