package com.druc.mateusz.moviecharactersdemo.api.model

data class Data<T>(
    val data: T
)