package com.druc.mateusz.moviecharactersdemo.ui.splash

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.druc.mateusz.moviecharactersdemo.BaseFragment
import com.druc.mateusz.moviecharactersdemo.databinding.FragmentSplashBinding
import kotlinx.coroutines.*
import org.koin.android.viewmodel.ext.android.viewModel

class SplashFragment: BaseFragment<FragmentSplashBinding>(FragmentSplashBinding::inflate) {

    private val viewModel by viewModel<SplashViewModel>()
    private lateinit var job: CompletableJob

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(!::job.isInitialized) { initJob() }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.progressBar.maxProgress = PROGRESS_BAR_MAX_PROGRESS.toFloat()
        startProgressLoading()
    }

    private fun startProgressLoading() {
        viewLifecycleOwner.lifecycleScope.launch {
            for (i in 0..PROGRESS_BAR_MAX_PROGRESS) {
                delay((SPLASH_DISPLAYING_DURATION / PROGRESS_BAR_MAX_PROGRESS).toLong())
                binding.progressBar.progress++
            }
            GlobalScope.launch(Dispatchers.Main) {
                viewModel.navigateToCharactersList()
            }
        }
    }

    private fun initJob() {
        job = Job()
    }

    companion object {
        const val SPLASH_DISPLAYING_DURATION = 5000
        const val PROGRESS_BAR_MAX_PROGRESS = 1000
    }
}