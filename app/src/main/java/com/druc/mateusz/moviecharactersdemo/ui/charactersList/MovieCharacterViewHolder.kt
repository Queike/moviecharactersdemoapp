package com.druc.mateusz.moviecharactersdemo.ui.charactersList

import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.druc.mateusz.moviecharactersdemo.BaseViewHolder
import com.druc.mateusz.moviecharactersdemo.R
import com.druc.mateusz.moviecharactersdemo.databinding.MovieCharacterListItemLayoutBinding
import com.druc.mateusz.moviecharactersdemo.domain.model.MovieCharacter
import com.druc.mateusz.moviecharactersdemo.inflateView

class MovieCharacterViewHolder(
    parent: ViewGroup,
    private val onClick: (listItem: MovieCharacter) -> Unit
): BaseViewHolder<MovieCharacter>(parent.inflateView(R.layout.movie_character_list_item_layout)) {

    override fun bind(item: MovieCharacter) {
        with(itemView) {
            val binding = DataBindingUtil.bind<MovieCharacterListItemLayoutBinding>(this.rootView)
            binding?.listItem = item
            binding?.itemCardView?.setOnClickListener { onClick.invoke(item) }
        }
    }
}