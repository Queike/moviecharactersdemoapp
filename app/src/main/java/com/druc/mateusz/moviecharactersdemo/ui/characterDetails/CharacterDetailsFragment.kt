package com.druc.mateusz.moviecharactersdemo.ui.characterDetails

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.druc.mateusz.moviecharactersdemo.BaseFragment
import com.druc.mateusz.moviecharactersdemo.databinding.FragmentCharacterDescriptionBinding
import com.druc.mateusz.moviecharactersdemo.domain.model.MovieCharacter
import org.koin.android.viewmodel.ext.android.viewModel

class CharacterDetailsFragment: BaseFragment<FragmentCharacterDescriptionBinding>(FragmentCharacterDescriptionBinding::inflate) {

    private val viewModel by viewModel<CharacterDetailsViewModel>()

    private val args: CharacterDetailsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        with(args) {
            binding.itemData = MovieCharacter(
                name = characterName,
                photoUrl = characterPhotoUrl,
                characterDescription = characterDescription
            )
        }
    }
}