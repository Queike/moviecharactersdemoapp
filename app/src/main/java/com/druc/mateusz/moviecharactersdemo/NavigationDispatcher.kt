package com.druc.mateusz.moviecharactersdemo

import android.app.Activity
import android.os.Bundle
import androidx.annotation.IdRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.*
import com.druc.mateusz.moviecharactersdemo.ui.charactersList.CharactersListFragmentDirections

class NavigationDispatcher {

    private val _navigationLiveData: MutableLiveData<NavigationEvent> by lazy { MutableLiveData<NavigationEvent>() }
    val navigationLiveData: LiveData<NavigationEvent> by lazy { _navigationLiveData }

    fun navigateToCharactersList() = navigateTo(R.id.action_splashFragment_to_charactersListFragment)

    fun navigateToCharacterDetails(characterName: String, photoUrl: String, characterDescription: String) {
        val action = CharactersListFragmentDirections.actionCharactersListFragmentToCharacterDetailsFragment(
            characterName = characterName,
            characterPhotoUrl = photoUrl,
            characterDescription = characterDescription
        )
        _navigationLiveData.value = NavigationEvent.Action { navigateTo(action) }
    }

    private fun navigateTo(navigationId: Int) {
        _navigationLiveData.value = NavigationEvent.Action {
            val navController = Navigation.findNavController(it, R.id.fragment_container)
            navController.navigateSafe(navigationId)
        }
    }

    private fun navigateTo(navDirections: NavDirections) {
        _navigationLiveData.value = NavigationEvent.Action {
            val navController = Navigation.findNavController(it, R.id.fragment_container)
            navController.navigateSafe(navDirections)
        }
    }

    private fun NavController.navigateSafe(
        navDirections: NavDirections
    ) {
        val action = currentDestination?.getAction(navDirections.actionId)
        if (action != null) navigate(navDirections)
    }

    private fun NavController.navigateSafe(
        @IdRes resId: Int,
        args: Bundle? = null,
        navOptions: NavOptions? = null,
        navExtras: Navigator.Extras? = null
    ) {
        val action = currentDestination?.getAction(resId)
        if (action != null) navigate(resId, args, navOptions, navExtras)
    }
}

sealed class NavigationEvent {
    object Empty : NavigationEvent()
    data class Action(val action: NavigationAction) : NavigationEvent()
}

typealias NavigationAction = (Activity) -> Unit