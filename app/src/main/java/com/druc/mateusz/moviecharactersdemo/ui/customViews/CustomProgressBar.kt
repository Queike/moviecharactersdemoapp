package com.druc.mateusz.moviecharactersdemo.ui.customViews

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

class CustomProgressBar(
    context: Context,
    attr: AttributeSet? = null
): View(context, attr) {

    private val paint: Paint = Paint()
    var maxProgress: Float = 100f
    var progress = 0
        set(value) {
            field = value
            invalidate()
        }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        paint.strokeWidth = 7f
        val width = width
        val height = 40f

        paint.textSize = 35f
        paint.color = Color.GRAY
        canvas?.drawLine(20f + (width - 40) * progress/maxProgress, height/2f, width - 20f, height/2f, paint)
        paint.color = Color.WHITE
        canvas?.drawLine(20f, height/2f, 20 + (width - 40) * progress/maxProgress, height/2f, paint)
        canvas?.drawCircle(20 +  (width - 40) * progress/maxProgress, height/2f, 10f, paint)
        canvas?.drawText("${(progress/(maxProgress/100)).toInt()} %", (width - 60) * progress/maxProgress, height + 25f, paint)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec)
    }
}