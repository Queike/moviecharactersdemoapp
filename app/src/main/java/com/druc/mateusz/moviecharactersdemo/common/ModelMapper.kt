package com.druc.mateusz.moviecharactersdemo.common

import com.druc.mateusz.moviecharactersdemo.api.model.MovieCharacterEntity
import com.druc.mateusz.moviecharactersdemo.domain.model.MovieCharacter

fun List<MovieCharacterEntity>.toDomain() = this.map {
    MovieCharacter(
        name = "${it.firstName} ${it.lastName}",
        photoUrl = it.photoUrl,
        characterDescription = it.characterDescription
    )
}