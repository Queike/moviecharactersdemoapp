package com.druc.mateusz.moviecharactersdemo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel: ViewModel() {

    val errorLiveData = MutableLiveData<String>()
}