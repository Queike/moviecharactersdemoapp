package com.druc.mateusz.moviecharactersdemo.api

import com.druc.mateusz.moviecharactersdemo.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private fun httpInterceptor() = HttpLoggingInterceptor().apply {
    level = HttpLoggingInterceptor.Level.BODY
}

private fun basicOkHttpClient() = OkHttpClient.Builder().addInterceptor(httpInterceptor()).build()

fun createBasicAuthService(): MovieCharactersApi {
    val retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .client(basicOkHttpClient())
        .baseUrl(BuildConfig.BASE_URL)
        .build()
    return retrofit.create(MovieCharactersApi::class.java)
}