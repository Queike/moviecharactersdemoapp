package com.druc.mateusz.moviecharactersdemo.ui.charactersList

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.druc.mateusz.moviecharactersdemo.BaseFragment
import com.druc.mateusz.moviecharactersdemo.common.observeChanges
import com.druc.mateusz.moviecharactersdemo.databinding.FragmentCharactersListBinding
import org.koin.android.viewmodel.ext.android.viewModel

class CharactersListFragment: BaseFragment<FragmentCharactersListBinding>(FragmentCharactersListBinding::inflate) {

    private val viewModel by viewModel<CharactersListViewModel>()
    private val charactersListAdapter by lazy { CharactersListAdapter(viewModel::navigateToItemDetails) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        setupRecyclerView()
        setupObservers()
    }

    private fun setupObservers() {
        observeErrors(viewModel.errorLiveData)
        observeCharactersData()
    }

    private fun setupRecyclerView() {
        with(binding.movieCharactersRecyclerView) {
            adapter = charactersListAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun observeCharactersData() {
        viewModel.charactersLiveData.observeChanges(viewLifecycleOwner) {
            charactersListAdapter.submitList(it)
            charactersListAdapter.notifyDataSetChanged()
        }
    }
}