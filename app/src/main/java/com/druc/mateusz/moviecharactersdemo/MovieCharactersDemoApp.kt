package com.druc.mateusz.moviecharactersdemo

import android.app.Application
import com.druc.mateusz.moviecharactersdemo.di.appModule
import com.druc.mateusz.moviecharactersdemo.di.networkModule
import com.druc.mateusz.moviecharactersdemo.di.repositoryModule
import com.druc.mateusz.moviecharactersdemo.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class MovieCharactersDemoApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@MovieCharactersDemoApp)
            modules(listOf(networkModule, appModule, repositoryModule, viewModelModule))
        }
        Timber.plant(Timber.DebugTree())
    }

}