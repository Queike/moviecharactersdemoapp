package com.druc.mateusz.moviecharactersdemo.ui.charactersList

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.druc.mateusz.moviecharactersdemo.BaseViewHolder
import com.druc.mateusz.moviecharactersdemo.domain.model.MovieCharacter

class CharactersListAdapter(
    private val onClick: (listItem: MovieCharacter) -> Unit
) : ListAdapter<MovieCharacter, BaseViewHolder<MovieCharacter>>(DiffCallback()) {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<MovieCharacter> {
        return MovieCharacterViewHolder(parent, onClick)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<MovieCharacter>, position: Int) {
        holder.bind(getItem(position))
    }
}

class DiffCallback: DiffUtil.ItemCallback<MovieCharacter>() {

    override fun areItemsTheSame(
        oldItem: MovieCharacter,
        newItem: MovieCharacter
    ): Boolean {
        return oldItem.name == newItem.name
                && oldItem.photoUrl == newItem.photoUrl
    }

    override fun areContentsTheSame(
        oldItem: MovieCharacter,
        newItem: MovieCharacter
    ): Boolean = oldItem == newItem
}