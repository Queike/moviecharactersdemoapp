package com.druc.mateusz.moviecharactersdemo.ui.splash

import androidx.lifecycle.ViewModel
import com.druc.mateusz.moviecharactersdemo.NavigationDispatcher

class SplashViewModel(
    private val navigationDispatcher: NavigationDispatcher
) : ViewModel() {

    fun navigateToCharactersList() {
        navigationDispatcher.navigateToCharactersList()
    }
}