package com.druc.mateusz.moviecharactersdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val navigationDispatcher by inject<NavigationDispatcher>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        observeNavigationEvents()
    }

    private fun observeNavigationEvents() {
        navigationDispatcher.navigationLiveData.observe(this) {
            if (it is NavigationEvent.Action) {
                it.action(this)
            }
        }
    }
}