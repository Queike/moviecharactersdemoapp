package com.druc.mateusz.moviecharactersdemo.domain.model

data class MovieCharacter(
    val name: String,
    val photoUrl: String,
    val characterDescription: String
)