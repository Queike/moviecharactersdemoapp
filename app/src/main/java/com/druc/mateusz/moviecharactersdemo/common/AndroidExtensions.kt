package com.druc.mateusz.moviecharactersdemo.common

import android.content.Context
import android.widget.ImageView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions

inline fun <reified T> LiveData<T>.observeChanges(
    lifecycleOwner: LifecycleOwner,
    crossinline observer: (T) -> Unit
) {
    observe(lifecycleOwner, Observer { observer(it) })
}

fun ImageView.loadOvalWithThumbnailFromUrl(imageUrl: String, viewLifecycleOwner: Context) {
    if (imageUrl.isNotEmpty()) {
        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)

        Glide
            .with(viewLifecycleOwner)
            .load(imageUrl)
            .apply(requestOptions)
            .thumbnail(0.3f)
            .transition(DrawableTransitionOptions.withCrossFade())
            .apply(RequestOptions.circleCropTransform())
            .into(this)
    }
}

fun ImageView.loadFromUrl(imageUrl: String, viewLifecycleOwner: Context) {
    if (imageUrl.isNotEmpty()) {
        Glide.with(viewLifecycleOwner).load(imageUrl).into(this)
    }
}