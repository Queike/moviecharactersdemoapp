package com.druc.mateusz.moviecharactersdemo.common

import android.content.Context
import com.druc.mateusz.moviecharactersdemo.R
import timber.log.Timber
import java.lang.Exception
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class ErrorMessageProvider (
    private val context: Context
) {
    fun getMessageForApiException(exception: Exception): String {
        Timber.e(exception)
        return when (exception) {
            is SocketTimeoutException -> context.getString(R.string.socket_timeout_exception_message)
            is UnknownHostException -> context.getString(R.string.unknown_host_exception_message)
            else -> context.getString(R.string.unknown_error_message, exception)
        }
    }
}